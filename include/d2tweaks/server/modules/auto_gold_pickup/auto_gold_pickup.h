#pragma once

#include <d2tweaks/server/modules/server_module.h>
#include <string>

namespace d2_tweaks {
	namespace server {
		class server;

		namespace modules {
			class auto_gold_pickup final : public server_module {
			public:
				
				std::string name = "auto_gold_pickup";
				void init() override;
				std::string getName() override;
				void tick(diablo2::structures::game* game, diablo2::structures::unit* unit) override;
			};
		}
	}
}
