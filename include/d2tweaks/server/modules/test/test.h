#pragma once

#include <d2tweaks/server/modules/server_module.h>
#include <string>

namespace d2_tweaks {
	namespace server {
		class server;

		namespace modules {
			class test final : public server_module {
			public:
				std::string name = "test";
				void init() override;
				std::string getName() override;
			};
		}
	}
}
