#pragma once

#include <d2tweaks/client/modules/client_module.h>
#include <string>

namespace d2_tweaks {
	namespace client {
		namespace modules {
			class loot_filter final : public client_module {
			public:
				std::string name = "loot_filter";
				void init() override;
				std::string getName() override;
			};
		}
	}
}
