#pragma once

#include <d2tweaks/client/modules/client_module.h>
#include <string>

//Inventory auto sort module client side

namespace d2_tweaks {
	namespace client {
		namespace modules {
			class autosort final : public client_module {
			public:
				std::string name = "autosort";
				std::string getName() override;
				void init() override;

				void handle_packet(common::packet_header* packet) override;
			};
		}
	}
}