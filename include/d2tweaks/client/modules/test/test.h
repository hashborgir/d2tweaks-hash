#pragma once

#include <d2tweaks/client/modules/client_module.h>
#include <string>

//Test client side module

namespace d2_tweaks {
	namespace client {
		namespace modules {
			class test final : public client_module {
			public:
				std::string name = "test";
				void init() override;
				std::string getName() override;
				void handle_packet(common::packet_header* packet) override;
			};
		}
	}
}