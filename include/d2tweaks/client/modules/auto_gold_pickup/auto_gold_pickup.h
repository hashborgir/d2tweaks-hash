#pragma once

#include <d2tweaks/client/modules/client_module.h>
#include <string>

namespace d2_tweaks {
	namespace client {
		namespace modules {
			class auto_gold_pickup final : public client_module {
			public:
				std::string name = "auto_gold_pickup";
				std::string getName() override;
				void init() override;

				void handle_packet(common::packet_header* packet) override;
			};
		}
	}
}