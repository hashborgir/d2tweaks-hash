#pragma once

#include <d2tweaks/client/modules/client_module.h>
#include <string>

//Client side patches that are too small to implement as separate modules

namespace d2_tweaks {
	namespace client {
		namespace modules {
			class small_patches final : public client_module {
			public:
				std::string name = "small_patches";
				void init() override;
				std::string getName() override;
			};
		}
	}
}