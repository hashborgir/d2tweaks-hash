#pragma once

#include <Windows.h>
#include <cstdint>

namespace diablo2 {
	namespace structures {
		struct unit;
		struct path;
		struct inventory;
		struct room;

		struct items_line;
		struct item_types_line;
	}

	enum unit_stats_t {
		UNIT_STAT_STRENGTH = 0,
		UNIT_STAT_ENERGY = 1,
		UNIT_STAT_DEXTERITY = 2,
		UNIT_STAT_VITALITY = 3,
		UNIT_STAT_STATPTS = 4,
		UNIT_STAT_NEWSKILLS = 5,
		UNIT_STAT_HITPOINTS = 6,
		UNIT_STAT_MAXHP = 7,
		UNIT_STAT_MANA = 8,
		UNIT_STAT_MAXMANA = 9,
		UNIT_STAT_STAMINA = 10,
		UNIT_STAT_MAXSTAMINA = 11,
		UNIT_STAT_LEVEL = 12,
		UNIT_STAT_EXPERIENCE = 13,
		UNIT_STAT_GOLD = 14,
		UNIT_STAT_GOLDBANK = 15,
		UNIT_STAT_ITEM_ARMOR_PERCENT = 16,
		UNIT_STAT_ITEM_MAXDAMAGE_PERCENT = 17,
		UNIT_STAT_ITEM_MINDAMAGE_PERCENT = 18,
		UNIT_STAT_TOHIT = 19,
		UNIT_STAT_TOBLOCK = 20,
		UNIT_STAT_MINDAMAGE = 21,
		UNIT_STAT_MAXDAMAGE = 22,
		UNIT_STAT_SECONDARY_MINDAMAGE = 23,
		UNIT_STAT_SECONDARY_MAXDAMAGE = 24,
		UNIT_STAT_DAMAGEPERCENT = 25,
		UNIT_STAT_MANARECOVERY = 26,
		UNIT_STAT_MANARECOVERYBONUS = 27,
		UNIT_STAT_STAMINARECOVERYBONUS = 28,
		UNIT_STAT_LASTEXP = 29,
		UNIT_STAT_NEXTEXP = 30,
		UNIT_STAT_ARMORCLASS = 31,
		UNIT_STAT_ARMORCLASS_VS_MISSILE = 32,
		UNIT_STAT_ARMORCLASS_VS_HTH = 33,
		UNIT_STAT_NORMAL_DAMAGE_REDUCTION = 34,
		UNIT_STAT_MAGIC_DAMAGE_REDUCTION = 35,
		UNIT_STAT_DAMAGERESIST = 36,
		UNIT_STAT_MAGICRESIST = 37,
		UNIT_STAT_MAXMAGICRESIST = 38,
		UNIT_STAT_FIRERESIST = 39,
		UNIT_STAT_MAXFIRERESIST = 40,
		UNIT_STAT_LIGHTRESIST = 41,
		UNIT_STAT_MAXLIGHTRESIST = 42,
		UNIT_STAT_COLDRESIST = 43,
		UNIT_STAT_MAXCOLDRESIST = 44,
		UNIT_STAT_POISONRESIST = 45,
		UNIT_STAT_MAXPOISONRESIST = 46,
		UNIT_STAT_DAMAGEAURA = 47,
		UNIT_STAT_FIREMINDAM = 48,
		UNIT_STAT_FIREMAXDAM = 49,
		UNIT_STAT_LIGHTMINDAM = 50,
		UNIT_STAT_LIGHTMAXDAM = 51,
		UNIT_STAT_MAGICMINDAM = 52,
		UNIT_STAT_MAGICMAXDAM = 53,
		UNIT_STAT_COLDMINDAM = 54,
		UNIT_STAT_COLDMAXDAM = 55,
		UNIT_STAT_COLDLENGTH = 56,
		UNIT_STAT_POISONMINDAM = 57,
		UNIT_STAT_POISONMAXDAM = 58,
		UNIT_STAT_POISONLENGTH = 59,
		UNIT_STAT_LIFEDRAINMINDAM = 60,
		UNIT_STAT_LIFEDRAINMAXDAM = 61,
		UNIT_STAT_MANADRAINMINDAM = 62,
		UNIT_STAT_MANADRAINMAXDAM = 63,
		UNIT_STAT_STAMDRAINMINDAM = 64,
		UNIT_STAT_STAMDRAINMAXDAM = 65,
		UNIT_STAT_STUNLENGTH = 66,
		UNIT_STAT_VELOCITYPERCENT = 67,
		UNIT_STAT_ATTACKRATE = 68,
		UNIT_STAT_OTHER_ANIMRATE = 69,
		UNIT_STAT_QUANTITY = 70,
		UNIT_STAT_VALUE = 71,
		UNIT_STAT_DURABILITY = 72,
		UNIT_STAT_MAXDURABILITY = 73,
		UNIT_STAT_HPREGEN = 74,
		UNIT_STAT_ITEM_MAXDURABILITY_PERCENT = 75,
		UNIT_STAT_ITEM_MAXHP_PERCENT = 76,
		UNIT_STAT_ITEM_MAXMANA_PERCENT = 77,
		UNIT_STAT_ITEM_ATTACKERTAKESDAMAGE = 78,
		UNIT_STAT_ITEM_GOLDBONUS = 79,
		UNIT_STAT_ITEM_MAGICBONUS = 80,
		UNIT_STAT_ITEM_KNOCKBACK = 81,
		UNIT_STAT_ITEM_TIMEDURATION = 82,
		UNIT_STAT_ITEM_ADDCLASSSKILLS = 83,
		UNIT_STAT_UNSENTPARAM1 = 84,
		UNIT_STAT_ITEM_ADDEXPERIENCE = 85,
		UNIT_STAT_ITEM_HEALAFTERKILL = 86,
		UNIT_STAT_ITEM_REDUCEDPRICES = 87,
		UNIT_STAT_ITEM_DOUBLEHERBDURATION = 88,
		UNIT_STAT_ITEM_LIGHTRADIUS = 89,
		UNIT_STAT_ITEM_LIGHTCOLOR = 90,
		UNIT_STAT_ITEM_REQ_PERCENT = 91,
		UNIT_STAT_ITEM_LEVELREQ = 92,
		UNIT_STAT_ITEM_FASTERATTACKRATE = 93,
		UNIT_STAT_ITEM_LEVELREQPCT = 94,
		UNIT_STAT_LASTBLOCKFRAME = 95,
		UNIT_STAT_ITEM_FASTERMOVEVELOCITY = 96,
		UNIT_STAT_ITEM_NONCLASSSKILL = 97,
		UNIT_STAT_STATE = 98,
		UNIT_STAT_ITEM_FASTERGETHITRATE = 99,
		UNIT_STAT_MONSTER_PLAYERCOUNT = 100,
		UNIT_STAT_SKILL_POISON_OVERRIDE_LENGTH = 101,
		UNIT_STAT_ITEM_FASTERBLOCKRATE = 102,
		UNIT_STAT_SKILL_BYPASS_UNDEAD = 103,
		UNIT_STAT_SKILL_BYPASS_DEMONS = 104,
		UNIT_STAT_ITEM_FASTERCASTRATE = 105,
		UNIT_STAT_SKILL_BYPASS_BEASTS = 106,
		UNIT_STAT_ITEM_SINGLESKILL = 107,
		UNIT_STAT_ITEM_RESTINPEACE = 108,
		UNIT_STAT_CURSE_RESISTANCE = 109,
		UNIT_STAT_ITEM_POISONLENGTHRESIST = 110,
		UNIT_STAT_ITEM_NORMALDAMAGE = 111,
		UNIT_STAT_ITEM_HOWL = 112,
		UNIT_STAT_ITEM_STUPIDITY = 113,
		UNIT_STAT_ITEM_DAMAGETOMANA = 114,
		UNIT_STAT_ITEM_IGNORETARGETAC = 115,
		UNIT_STAT_ITEM_FRACTIONALTARGETAC = 116,
		UNIT_STAT_ITEM_PREVENTHEAL = 117,
		UNIT_STAT_ITEM_HALFFREEZEDURATION = 118,
		UNIT_STAT_ITEM_TOHIT_PERCENT = 119,
		UNIT_STAT_ITEM_DAMAGETARGETAC = 120,
		UNIT_STAT_ITEM_DEMONDAMAGE_PERCENT = 121,
		UNIT_STAT_ITEM_UNDEADDAMAGE_PERCENT = 122,
		UNIT_STAT_ITEM_DEMON_TOHIT = 123,
		UNIT_STAT_ITEM_UNDEAD_TOHIT = 124,
		UNIT_STAT_ITEM_THROWABLE = 125,
		UNIT_STAT_ITEM_ELEMSKILL = 126,
		UNIT_STAT_ITEM_ALLSKILLS = 127,
		UNIT_STAT_ITEM_ATTACKERTAKESLIGHTDAMAGE = 128,
		UNIT_STAT_IRONMAIDEN_LEVEL = 129,
		UNIT_STAT_LIFETAP_LEVEL = 130,
		UNIT_STAT_THORNS_PERCENT = 131,
		UNIT_STAT_BONEARMOR = 132,
		UNIT_STAT_BONEARMORMAX = 133,
		UNIT_STAT_ITEM_FREEZE = 134,
		UNIT_STAT_ITEM_OPENWOUNDS = 135,
		UNIT_STAT_ITEM_CRUSHINGBLOW = 136,
		UNIT_STAT_ITEM_KICKDAMAGE = 137,
		UNIT_STAT_ITEM_MANAAFTERKILL = 138,
		UNIT_STAT_ITEM_HEALAFTERDEMONKILL = 139,
		UNIT_STAT_ITEM_EXTRABLOOD = 140,
		UNIT_STAT_ITEM_DEADLYSTRIKE = 141,
		UNIT_STAT_ITEM_ABSORBFIRE_PERCENT = 142,
		UNIT_STAT_ITEM_ABSORBFIRE = 143,
		UNIT_STAT_ITEM_ABSORBLIGHT_PERCENT = 144,
		UNIT_STAT_ITEM_ABSORBLIGHT = 145,
		UNIT_STAT_ITEM_ABSORBMAGIC_PERCENT = 146,
		UNIT_STAT_ITEM_ABSORBMAGIC = 147,
		UNIT_STAT_ITEM_ABSORBCOLD_PERCENT = 148,
		UNIT_STAT_ITEM_ABSORBCOLD = 149,
		UNIT_STAT_ITEM_SLOW = 150,
		UNIT_STAT_ITEM_AURA = 151,
		UNIT_STAT_ITEM_INDESCTRUCTIBLE = 152,
		UNIT_STAT_ITEM_CANNOTBEFROZEN = 153,
		UNIT_STAT_ITEM_STAMINADRAINPCT = 154,
		UNIT_STAT_ITEM_REANIMATE = 155,
		UNIT_STAT_ITEM_PIERCE = 156,
		UNIT_STAT_ITEM_MAGICARROW = 157,
		UNIT_STAT_ITEM_EXPLOSIVEARROW = 158,
		UNIT_STAT_ITEM_THROW_MINDAMAGE = 159,
		UNIT_STAT_ITEM_THROW_MAXDAMAGE = 160,
		UNIT_STAT_SKILL_HANDOFATHENA = 161,
		UNIT_STAT_SKILL_STAMINAPERCENT = 162,
		UNIT_STAT_SKILL_PASSIVE_STAMINAPERCENT = 163,
		UNIT_STAT_SKILL_CONCENTRATION = 164,
		UNIT_STAT_SKILL_ENCHANT = 165,
		UNIT_STAT_SKILL_PIERCE = 166,
		UNIT_STAT_SKILL_CONVICTION = 167,
		UNIT_STAT_SKILL_CHILLINGARMOR = 168,
		UNIT_STAT_SKILL_FRENZY = 169,
		UNIT_STAT_SKILL_DECREPIFY = 170,
		UNIT_STAT_SKILL_ARMOR_PERCENT = 171,
		UNIT_STAT_ALIGNMENT = 172,
		UNIT_STAT_TARGET0 = 173,
		UNIT_STAT_TARGET1 = 174,
		UNIT_STAT_GOLDLOST = 175,
		UNIT_STAT_CONVERSION_LEVEL = 176,
		UNIT_STAT_CONVERSION_MAXHP = 177,
		UNIT_STAT_UNIT_DOOVERLAY = 178,
		UNIT_STAT_ATTACK_VS_MONTYPE = 179,
		UNIT_STAT_DAMAGE_VS_MONTYPE = 180,
		UNIT_STAT_FADE = 181,
		UNIT_STAT_ARMOR_OVERRIDE_PERCENT = 182,
		UNIT_STAT_KILLCOUNTER = 183,
		UNIT_STAT_SOULSCAPTURED = 184,
		UNIT_STAT_SPIRITS = 185,
		UNIT_STAT_SKILL_MORE = 186,
		UNIT_STAT_ITEM_CORRUPTED = 187,
		UNIT_STAT_ITEM_ADDSKILL_TAB = 188,
		UNIT_STAT_HIDDEN_CORRUPTION = 189,
		UNIT_STAT_ITEM_STRENGTH_SPIRITS = 190,
		UNIT_STAT_ITEM_DEXTERITY_SPIRITS = 191,
		UNIT_STAT_ITEM_VITALITY_SPIRITS = 192,
		UNIT_STAT_ITEM_ENERGY_SPIRITS = 193,
		UNIT_STAT_ITEM_NUMSOCKETS = 194,
		UNIT_STAT_ITEM_SKILLONATTACK = 195,
		UNIT_STAT_ITEM_SKILLONKILL = 196,
		UNIT_STAT_ITEM_SKILLONDEATH = 197,
		UNIT_STAT_ITEM_SKILLONHIT = 198,
		UNIT_STAT_ITEM_SKILLONLEVELUP = 199,
		UNIT_STAT_ITEM_SKILL_SOULS = 200,
		UNIT_STAT_ITEM_SKILLONGETHIT = 201,
		UNIT_STAT_UNUSED202 = 202,
		UNIT_STAT_UNUSED203 = 203,
		UNIT_STAT_ITEM_CHARGED_SKILL = 204,
		UNIT_STAT_UNUSED204 = 205,
		UNIT_STAT_UNUSED205 = 206,
		UNIT_STAT_UNUSED206 = 207,
		UNIT_STAT_UNUSED207 = 208,
		UNIT_STAT_UNUSED208 = 209,
		UNIT_STAT_UNUSED209 = 210,
		UNIT_STAT_UNUSED210 = 211,
		UNIT_STAT_UNUSED211 = 212,
		UNIT_STAT_UNUSED212 = 213,
		UNIT_STAT_ITEM_ARMOR_PERLEVEL = 214,
		UNIT_STAT_ITEM_ARMORPERCENT_PERLEVEL = 215,
		UNIT_STAT_ITEM_HP_PERLEVEL = 216,
		UNIT_STAT_ITEM_MANA_PERLEVEL = 217,
		UNIT_STAT_ITEM_MAXDAMAGE_PERLEVEL = 218,
		UNIT_STAT_ITEM_MAXDAMAGE_PERCENT_PERLEVEL = 219,
		UNIT_STAT_ITEM_STRENGTH_PERLEVEL = 220,
		UNIT_STAT_ITEM_DEXTERITY_PERLEVEL = 221,
		UNIT_STAT_ITEM_ENERGY_PERLEVEL = 222,
		UNIT_STAT_ITEM_VITALITY_PERLEVEL = 223,
		UNIT_STAT_ITEM_TOHIT_PERLEVEL = 224,
		UNIT_STAT_ITEM_TOHITPERCENT_PERLEVEL = 225,
		UNIT_STAT_ITEM_COLD_DAMAGEMAX_PERLEVEL = 226,
		UNIT_STAT_ITEM_FIRE_DAMAGEMAX_PERLEVEL = 227,
		UNIT_STAT_ITEM_LTNG_DAMAGEMAX_PERLEVEL = 228,
		UNIT_STAT_ITEM_POIS_DAMAGEMAX_PERLEVEL = 229,
		UNIT_STAT_ITEM_RESIST_COLD_PERLEVEL = 230,
		UNIT_STAT_ITEM_RESIST_FIRE_PERLEVEL = 231,
		UNIT_STAT_ITEM_RESIST_LTNG_PERLEVEL = 232,
		UNIT_STAT_ITEM_RESIST_POIS_PERLEVEL = 233,
		UNIT_STAT_ITEM_ABSORB_COLD_PERLEVEL = 234,
		UNIT_STAT_ITEM_ABSORB_FIRE_PERLEVEL = 235,
		UNIT_STAT_ITEM_ABSORB_LTNG_PERLEVEL = 236,
		UNIT_STAT_ITEM_ABSORB_POIS_PERLEVEL = 237,
		UNIT_STAT_ITEM_THORNS_PERLEVEL = 238,
		UNIT_STAT_ITEM_FIND_GOLD_PERLEVEL = 239,
		UNIT_STAT_ITEM_FIND_MAGIC_PERLEVEL = 240,
		UNIT_STAT_ITEM_REGENSTAMINA_PERLEVEL = 241,
		UNIT_STAT_ITEM_STAMINA_PERLEVEL = 242,
		UNIT_STAT_ITEM_DAMAGE_DEMON_PERLEVEL = 243,
		UNIT_STAT_ITEM_DAMAGE_UNDEAD_PERLEVEL = 244,
		UNIT_STAT_ITEM_TOHIT_DEMON_PERLEVEL = 245,
		UNIT_STAT_ITEM_TOHIT_UNDEAD_PERLEVEL = 246,
		UNIT_STAT_ITEM_CRUSHINGBLOW_PERLEVEL = 247,
		UNIT_STAT_ITEM_OPENWOUNDS_PERLEVEL = 248,
		UNIT_STAT_ITEM_KICK_DAMAGE_PERLEVEL = 249,
		UNIT_STAT_ITEM_DEADLYSTRIKE_PERLEVEL = 250,
		UNIT_STAT_ITEM_FIND_GEMS_PERLEVEL = 251,
		UNIT_STAT_ITEM_REPLENISH_DURABILITY = 252,
		UNIT_STAT_ITEM_REPLENISH_QUANTITY = 253,
		UNIT_STAT_ITEM_EXTRA_STACK = 254,
		UNIT_STAT_ITEM_FIND_ITEM = 255,
		UNIT_STAT_ITEM_SLASH_DAMAGE = 256,
		UNIT_STAT_ITEM_SLASH_DAMAGE_PERCENT = 257,
		UNIT_STAT_ITEM_CRUSH_DAMAGE = 258,
		UNIT_STAT_ITEM_CRUSH_DAMAGE_PERCENT = 259,
		UNIT_STAT_ITEM_THRUST_DAMAGE = 260,
		UNIT_STAT_ITEM_THRUST_DAMAGE_PERCENT = 261,
		UNIT_STAT_ITEM_ABSORB_SLASH = 262,
		UNIT_STAT_ITEM_ABSORB_CRUSH = 263,
		UNIT_STAT_ITEM_ABSORB_THRUST = 264,
		UNIT_STAT_ITEM_ABSORB_SLASH_PERCENT = 265,
		UNIT_STAT_ITEM_ABSORB_CRUSH_PERCENT = 266,
		UNIT_STAT_ITEM_ABSORB_THRUST_PERCENT = 267,
		UNIT_STAT_UNUSED213 = 268,
		UNIT_STAT_UNUSED214 = 269,
		UNIT_STAT_UNUSED215 = 270,
		UNIT_STAT_UNUSED216 = 271,
		UNIT_STAT_UNUSED217 = 272,
		UNIT_STAT_UNUSED218 = 273,
		UNIT_STAT_UNUSED219 = 274,
		UNIT_STAT_UNUSED220 = 275,
		UNIT_STAT_UNUSED221 = 276,
		UNIT_STAT_UNUSED222 = 277,
		UNIT_STAT_UNUSED223 = 278,
		UNIT_STAT_UNUSED224 = 279,
		UNIT_STAT_UNUSED225 = 280,
		UNIT_STAT_UNUSED226 = 281,
		UNIT_STAT_UNUSED227 = 282,
		UNIT_STAT_UNUSED228 = 283,
		UNIT_STAT_UNUSED229 = 284,
		UNIT_STAT_UNUSED230 = 285,
		UNIT_STAT_UNUSED231 = 286,
		UNIT_STAT_UNUSED232 = 287,
		UNIT_STAT_UNUSED233 = 288,
		UNIT_STAT_UNUSED234 = 289,
		UNIT_STAT_UNUSED235 = 290,
		UNIT_STAT_UNUSED236 = 291,
		UNIT_STAT_UNUSED237 = 292,
		UNIT_STAT_UNUSED238 = 293,
		UNIT_STAT_UNUSED239 = 294,
		UNIT_STAT_UNUSED240 = 295,
		UNIT_STAT_UNUSED241 = 296,
		UNIT_STAT_UNUSED242 = 297,
		UNIT_STAT_UNUSED243 = 298,
		UNIT_STAT_DISPLAY_SPIRITS = 299,
		UNIT_STAT_DISPLAY_SOULS_CAPTURED = 300,
		UNIT_STAT_MAGHARV = 301,
		UNIT_STAT_DUMMY = 302,
		UNIT_STAT_DISPLAY_KILLS = 303,
		UNIT_STAT_IFORGE = 304,
		UNIT_STAT_ITEM_PIERCE_COLD = 305,
		UNIT_STAT_ITEM_PIERCE_FIRE = 306,
		UNIT_STAT_ITEM_PIERCE_LTNG = 307,
		UNIT_STAT_ITEM_PIERCE_POIS = 308,
		UNIT_STAT_ITEM_DAMAGE_VS_MONSTER = 309,
		UNIT_STAT_ITEM_DAMAGE_PERCENT_VS_MONSTER = 310,
		UNIT_STAT_ITEM_TOHIT_VS_MONSTER = 311,
		UNIT_STAT_ITEM_TOHIT_PERCENT_VS_MONSTER = 312,
		UNIT_STAT_ITEM_AC_VS_MONSTER = 313,
		UNIT_STAT_ITEM_AC_PERCENT_VS_MONSTER = 314,
		UNIT_STAT_FIRELENGTH = 315,
		UNIT_STAT_BURNINGMIN = 316,
		UNIT_STAT_BURNINGMAX = 317,
		UNIT_STAT_PROGRESSIVE_DAMAGE = 318,
		UNIT_STAT_PROGRESSIVE_STEAL = 319,
		UNIT_STAT_PROGRESSIVE_OTHER = 320,
		UNIT_STAT_PROGRESSIVE_FIRE = 321,
		UNIT_STAT_PROGRESSIVE_COLD = 322,
		UNIT_STAT_PROGRESSIVE_LIGHTNING = 323,
		UNIT_STAT_ITEM_EXTRA_CHARGES = 324,
		UNIT_STAT_PROGRESSIVE_TOHIT = 325,
		UNIT_STAT_POISON_COUNT = 326,
		UNIT_STAT_DAMAGE_FRAMERATE = 327,
		UNIT_STAT_PIERCE_IDX = 328,
		UNIT_STAT_PASSIVE_FIRE_MASTERY = 329,
		UNIT_STAT_PASSIVE_LTNG_MASTERY = 330,
		UNIT_STAT_PASSIVE_COLD_MASTERY = 331,
		UNIT_STAT_PASSIVE_POIS_MASTERY = 332,
		UNIT_STAT_PASSIVE_FIRE_PIERCE = 333,
		UNIT_STAT_PASSIVE_LTNG_PIERCE = 334,
		UNIT_STAT_PASSIVE_COLD_PIERCE = 335,
		UNIT_STAT_PASSIVE_POIS_PIERCE = 336,
		UNIT_STAT_PASSIVE_CRITICAL_STRIKE = 337,
		UNIT_STAT_PASSIVE_DODGE = 338,
		UNIT_STAT_PASSIVE_AVOID = 339,
		UNIT_STAT_PASSIVE_EVADE = 340,
		UNIT_STAT_PASSIVE_WARMTH = 341,
		UNIT_STAT_PASSIVE_MASTERY_MELEE_TH = 342,
		UNIT_STAT_PASSIVE_MASTERY_MELEE_DMG = 343,
		UNIT_STAT_PASSIVE_MASTERY_MELEE_CRIT = 344,
		UNIT_STAT_PASSIVE_MASTERY_THROW_TH = 345,
		UNIT_STAT_PASSIVE_MASTERY_THROW_DMG = 346,
		UNIT_STAT_PASSIVE_MASTERY_THROW_CRIT = 347,
		UNIT_STAT_PASSIVE_WEAPONBLOCK = 348,
		UNIT_STAT_PASSIVE_SUMMON_RESIST = 349,
		UNIT_STAT_MODIFIERLIST_SKILL = 350,
		UNIT_STAT_MODIFIERLIST_LEVEL = 351,
		UNIT_STAT_LAST_SENT_HP_PCT = 352,
		UNIT_STAT_SOURCE_UNIT_TYPE = 353,
		UNIT_STAT_SOURCE_UNIT_ID = 354,
		UNIT_STAT_SHORTPARAM1 = 355,
		UNIT_STAT_QUESTITEMDIFFICULTY = 356,
		UNIT_STAT_PASSIVE_MAG_MASTERY = 357,
		UNIT_STAT_PASSIVE_MAG_PIERCE = 358,
		UNIT_STAT_ITEM_STRENGTH_PERCENT = 359,
		UNIT_STAT_ITEM_DEXTERITY_PERCENT = 360,
		UNIT_STAT_ITEM_VITALITY_PERCENT = 361,
		UNIT_STAT_ITEM_ENERGY_PERCENT = 362,
		UNIT_STAT_ITEM_STRENGTHPERCENT_PERLEVEL = 363,
		UNIT_STAT_ITEM_DEXTERITYPERCENT_PERLEVEL = 364,
		UNIT_STAT_ITEM_ENERGYPERCENT_PERLEVEL = 365,
		UNIT_STAT_ITEM_VITALITYPERCENT_PERLEVEL = 366,
		UNIT_STAT_ITEM_ATTACKERGETSBLIND = 367,
		UNIT_STAT_ITEM_ATTACKERFLEES = 368,
		UNIT_STAT_ITEM_ATTACKERTAKESFIREDAMAGE = 369,
		UNIT_STAT_ITEM_ATTACKERTAKESCOLDDAMAGE = 370,
		UNIT_STAT_ITEM_MAXDAMAGE_PERSTR = 371,
		UNIT_STAT_ITEM_MAXDAMAGE_PERDEX = 372,
		UNIT_STAT_ITEM_MINDAMAGE_PERLVL = 373,
		UNIT_STAT_ITEM_MINDAMAGE_PERSTR = 374,
		UNIT_STAT_ITEM_MINDAMAGE_PERDEX = 375,
		UNIT_STAT_ITEM_MAXDAMAGE_PERCENT_PERSTR = 376,
		UNIT_STAT_ITEM_MAXDAMAGE_PERCENT_PERDEX = 377,
		UNIT_STAT_ITEM_OPENWOUNDS_PERDEX = 378,
		UNIT_STAT_ITEM_OPENWOUNDS_PERSTR = 379,
		UNIT_STAT_ITEM_DEADLYSTRIKE_PERDEX = 380,
		UNIT_STAT_ITEM_DEADLYSTRIKE_PERSTR = 381,
		UNIT_STAT_ITEM_ARMOR_PERSTR = 382,
		UNIT_STAT_ITEM_ARMOR_PERDEX = 383,
		UNIT_STAT_ITEM_TOHIT_PERSTR = 384,
		UNIT_STAT_ITEM_TOHIT_PERDEX = 385,
		UNIT_STAT_ITEM_HP_PERVITALITY = 386,
		UNIT_STAT_ITEM_MANA_PERENR = 387,
		UNIT_STAT_ITEM_FASTERCASTRATE_PERENR = 388,
		UNIT_STAT_ITEM_FASTERBLOCKRATE_PERDEX = 389,
		UNIT_STAT_ITEM_FASTERMOVEVELOCITY_PERVITALITY = 390,
		UNIT_STAT_ITEM_FASTERSWINGVELOCITY_PERSTRENGTH = 391,
		UNIT_STAT_ITEM_MINDAMAGE_PERCENT_PERSTR = 392,
		UNIT_STAT_ITEM_MINDAMAGE_PERCENT_PERDEX = 393,
		UNIT_STAT_ITEM_MINDAMAGE_PERCENT_PERLVL = 394,
		UNIT_STAT_ITEM_CRUSHINGBLOW_PERSTR = 395,
		UNIT_STAT_ITEM_CRUSHINGBLOW_PERDEX = 396,
		UNIT_STAT_ITEM_ELEMSKILLCOLD = 397,
		UNIT_STAT_ITEM_ELEMSKILLLIGHT = 398,
		UNIT_STAT_ITEM_ELEMSKILLPOISON = 399,
		UNIT_STAT_ITEM_ELEMSKILLMAGIC = 400,
		UNIT_STAT_ITEM_ELEMSKILLFIRE = 401,
		UNIT_STAT_ITEM_ARMORPERCENT_PERSTR = 402,
		UNIT_STAT_ITEM_ARMORPERCENT_PERDEX = 403,
		UNIT_STAT_ITEM_TOHITPERCENT_PERSTR = 404,
		UNIT_STAT_ITEM_TOHITPERCENT_PERDEX = 405,
		UNIT_STAT_ITEM_FASTERSWINGVELOCITY_PERDEX = 406,
		UNIT_STAT_ITEM_FASTERSWINGVELOCITY_PERLVL = 407,
		UNIT_STAT_ITEM_FASTERBLOCKRATE_PERSTR = 408,
		UNIT_STAT_ITEM_FASTERBLOCKRATE_PERLVL = 409,
		UNIT_STAT_ITEM_FASTERMOVEVELOCITY_PERDEX = 410,
		UNIT_STAT_ITEM_FASTERMOVEVELOCITY_PERLVL = 411,
		UNIT_STAT_ITEM_FASTERCASTRATE_PERLVL = 412,
		UNIT_STAT_ITEM_FASTERHITRECOVERY_PERVIT = 413,
		UNIT_STAT_ITEM_FASTERHITRECOVERY_PERLVL = 414,
		UNIT_STAT_ITEM_INCREASEDBLOCK_PERDEX = 415,
		UNIT_STAT_ITEM_INCREASEDBLOCK_PERLVL = 416,
		UNIT_STAT_ITEM_ADDEXPERIENCE_PERLVL = 417,
		UNIT_STAT_ITEM_ADDEXPERIENCE_PERENR = 418,
		UNIT_STAT_ITEM_REDUCEDPRICES_PERLVL = 419,
		UNIT_STAT_ITEM_PIERCE_PERLVL = 420,
		UNIT_STAT_ITEM_PIERCE_PERSTR = 421,
		UNIT_STAT_RESMAX_DISPLAY_ALL_ZERO = 422,
		UNIT_STAT_MAXMAGRES_PERCENT = 423,
		UNIT_STAT_MAXFIRERES_PERCENT = 424,
		UNIT_STAT_MAXLIGHTRES_PERCENT = 425,
		UNIT_STAT_MAXCOLDRES_PERCENT = 426,
		UNIT_STAT_MAXPOISRES_PERCENT = 427,
		UNIT_STAT_MAGICRESIST_HIDDEN = 428,
		UNIT_STAT_MAXMAGICRESIST_HIDDEN = 429,
		UNIT_STAT_FIRERESIST_HIDDEN = 430,
		UNIT_STAT_MAXFIRERESIST_HIDDEN = 431,
		UNIT_STAT_LIGHTRESIST_HIDDEN = 432,
		UNIT_STAT_MAXLIGHTRESIST_HIDDEN = 433,
		UNIT_STAT_COLDRESIST_HIDDEN = 434,
		UNIT_STAT_MAXCOLDRESIST_HIDDEN = 435,
		UNIT_STAT_POISONRESIST_HIDDEN = 436,
		UNIT_STAT_MAXPOISONRESIST_HIDDEN = 437,
		UNIT_STAT_HIDDENRESIST_DUMMY = 438,
		UNIT_STAT_ITEM_SLOW_PERLVL = 439,
		UNIT_STAT_ITEM_KICK_DAMAGE_PERSTR = 440,
		UNIT_STAT_ITEM_KICK_DAMAGE_PERDEX = 441,
		UNIT_STAT_DAMAGERESIST_PERLVL = 442,
		UNIT_STAT_DAMAGERESIST_PERVIT = 443,
		UNIT_STAT_MAGIC_DAMAGE_REDUCTION_PERLVL = 444,
		UNIT_STAT_MAGIC_DAMAGE_REDUCTION_PERENR = 445,
		UNIT_STAT_MAGICRESIST_PERLVL = 446,
		UNIT_STAT_MAGICRESIST_PERENR = 447,
		UNIT_STAT_ITEM_STUPIDITY_PERLVL = 448,
		UNIT_STAT_ITEM_FREEZE_PERLVL = 449,
		UNIT_STAT_ITEM_FREEZE_PERENR = 450,
		UNIT_STAT_ITEM_POISONLENGTHRESIST_PERLVL = 451,
		UNIT_STAT_ITEM_POISONLENGTHRESIST_PERVIT = 452,
		UNIT_STAT_CURSE_RESISTANCE_PERLVL = 453,
		UNIT_STAT_CURSE_RESISTANCE_PERENR = 454,
		UNIT_STAT_CURSE_RESISTANCE_LENGTH = 455,
		UNIT_STAT_ITEM_MANAAFTERKILL_PERLVL = 456,
		UNIT_STAT_ITEM_MANAAFTERKILL_PERENR = 457,
		UNIT_STAT_ITEM_DAMAGETOMANA_PERLVL = 458,
		UNIT_STAT_ITEM_DAMAGETOMANA_PERENR = 459,
		UNIT_STAT_ITEM_DAMAGETOMANA_PERMANA = 460,
		UNIT_STAT_UNUSED244 = 461,
		UNIT_STAT_GEMBAG_PRUBY = 462,
		UNIT_STAT_GEMBAG_FRUBY = 463,
		UNIT_STAT_GEMBAG_NRUBY = 464,
		UNIT_STAT_GEMBAG_FLRUBY = 465,
		UNIT_STAT_GEMBAG_CRUBY = 466,
		UNIT_STAT_GEMBAG_PAMETHYST = 467,
		UNIT_STAT_GEMBAG_FAMETHYST = 468,
		UNIT_STAT_GEMBAG_NAMETHYST = 469,
		UNIT_STAT_GEMBAG_FLAMETHYST = 470,
		UNIT_STAT_GEMBAG_CAMETHYST = 471,
		UNIT_STAT_GEMBAG_PDIAMOND = 472,
		UNIT_STAT_GEMBAG_FDIAMOND = 473,
		UNIT_STAT_GEMBAG_NDIAMOND = 474,
		UNIT_STAT_GEMBAG_FLDIAMOND = 475,
		UNIT_STAT_GEMBAG_CDIAMOND = 476,
		UNIT_STAT_GEMBAG_PEMERALD = 477,
		UNIT_STAT_GEMBAG_FEMERALD = 478,
		UNIT_STAT_GEMBAG_NEMERALD = 479,
		UNIT_STAT_GEMBAG_FLEMERALD = 480,
		UNIT_STAT_GEMBAG_CEMERALD = 481,
		UNIT_STAT_GEMBAG_PSAPPHIRE = 482,
		UNIT_STAT_GEMBAG_FSAPPHIRE = 483,
		UNIT_STAT_GEMBAG_NSAPPHIRE = 484,
		UNIT_STAT_GEMBAG_FLSAPPHIRE = 485,
		UNIT_STAT_GEMBAG_CSAPPHIRE = 486,
		UNIT_STAT_GEMBAG_PTOPAZ = 487,
		UNIT_STAT_GEMBAG_FTOPAZ = 488,
		UNIT_STAT_GEMBAG_NTOPAZ = 489,
		UNIT_STAT_GEMBAG_FLTOPAZ = 490,
		UNIT_STAT_GEMBAG_CTOPAZ = 491,
		UNIT_STAT_GEMBAG_PSKULL = 492,
		UNIT_STAT_GEMBAG_FSKULL = 493,
		UNIT_STAT_GEMBAG_NSKULL = 494,
		UNIT_STAT_GEMBAG_FLSKULL = 495,
		UNIT_STAT_GEMBAG_CSKULL = 496,
		UNIT_STAT_PASSIVE_SUM_MASTERY = 497,
		UNIT_STAT_ITEM_SOCKETMULTIPLIER = 498,
		UNIT_STAT_UNUSED245 = 499,
		UNIT_STAT_UNUSED246 = 500,
		UNIT_STAT_UNUSED247 = 501,
		UNIT_STAT_UNUSED248 = 502,
		UNIT_STAT_UNUSED249 = 503,
		UNIT_STAT_UNUSED250 = 504,
		UNIT_STAT_UNUSED251 = 505,
		UNIT_STAT_UNUSED252 = 506,
		UNIT_STAT_UNUSED253 = 507,
		UNIT_STAT_UNUSED254 = 508,
		UNIT_STAT_UNUSED255 = 509,
		UNIT_STAT_ITEM_AURA_DISPLAY = 510
	};

	class d2_common {
	public:
		static char* get_base();

		static int32_t get_inventory_index(structures::unit* item, char page, BOOL lod);
		static void* get_inventory_data(int32_t index, int32_t zero, char* data);
		static structures::unit* get_item_at_cell(structures::inventory* inv, uint32_t cellx, uint32_t celly,
												  uint32_t* pcellx, uint32_t* pcelly, int32_t invIndex, uint8_t page);
		static uint32_t can_put_into_slot(structures::inventory* inv, structures::unit* item, uint32_t x, uint32_t y,
										  uint32_t invIndex, structures::unit** lastBlockingUnit, uint32_t* lastBlockingUnitIndex, uint8_t page);

		static structures::unit* inv_remove_item(structures::inventory* inventory, structures::unit* item);
		static BOOL inv_add_item(structures::inventory* inv, structures::unit* item, uint32_t x, uint32_t y,
								 uint32_t invIndex, BOOL isClient, uint8_t page);
		static BOOL inv_update_item(structures::inventory* inv, structures::unit* item, BOOL isClient);

		static structures::items_line* get_item_record(uint32_t guid);
		static structures::item_types_line* get_item_type_record(uint32_t typeId);

		static uint32_t get_maximum_character_gold(structures::unit* player);

		static int32_t set_stat(structures::unit* unit, unit_stats_t stat, uint32_t value, int16_t param);
		static int32_t get_stat(structures::unit* unit, unit_stats_t stat, int16_t param);
		static int32_t get_stat_signed(structures::unit* unit, unit_stats_t stat, int16_t param);

		static int32_t _10111(int32_t* x, int32_t* y);
		static int32_t _10116(int32_t x1, int32_t y1, int32_t* x, int32_t* y);

		static structures::room* get_room_from_unit(structures::unit* unit);

		static int32_t get_unit_size_x(structures::unit* unit);
		static int32_t get_unit_size_y(structures::unit* unit);

		static int32_t get_distance_between_units(structures::unit* unit1, structures::unit* unit2);

		static int32_t get_unit_x(structures::path* path);
		static int32_t get_unit_y(structures::path* path);
		static int32_t get_unit_precise_x(structures::unit* unit);
		static int32_t get_unit_precise_y(structures::unit* unit);

		static structures::unit* get_target_from_path(structures::path* path);
	};
}
