#include <d2tweaks/client/modules/loot_filter/loot_filter_settings_toggle_menu.h>

#include <functional>

#include <d2tweaks/ui/controls/button.h>

#include <diablo2/d2client.h>
#include <d2tweaks/ui/ui_manager.h>
//debug junk
//#include <iostream>

d2_tweaks::client::modules::loot_filter_settings_toggle_menu::loot_filter_settings_toggle_menu(token) {
	m_show = false;
	//debug junk
	//std::cout << "loot filter settings toggle menu init\n";
	menu::set_enabled(true);
	menu::set_visible(true);
	//debug junk
	//std::cout << menu::get_visible() << " loot filter settings toggle menu visibility\n";
	load_xml("d2tweaks\\interfaces\\loot_filter_settings_toggle_menu.xml");

	m_toggle_filter_settings_btn = static_cast<ui::controls::button*>(
		get_control("m_toggle_filter_settings_btn"));
	m_toggle_filter_settings_btn->set_enabled(false);
	m_toggle_filter_settings_btn->set_visible(false);
	m_toggle_filter_settings_btn->set_on_click(std::bind(&loot_filter_settings_toggle_menu::toggle_filter_settings_click, this));
	//debug junk
	//std::cout << menu::get_visible() << " loot filter settings toggle menu visibility2\n";
	m_filter_settings_menu = singleton<ui::ui_manager>::instance().get_menu("loot_filter_settings_menu");
}

void d2_tweaks::client::modules::loot_filter_settings_toggle_menu::toggle_filter_settings_click() {
	m_show = !m_show;
	//debug junk
	//std::cout << "loot filter settings toggle menu click\n";
	m_filter_settings_menu->set_enabled(m_show);
	m_filter_settings_menu->set_visible(m_show);
}

void d2_tweaks::client::modules::loot_filter_settings_toggle_menu::draw() {
	m_toggle_filter_settings_btn->set_enabled(diablo2::d2_client::get_ui_window_state(diablo2::UI_WINDOW_MINIPANEL));
	m_toggle_filter_settings_btn->set_visible(diablo2::d2_client::get_ui_window_state(diablo2::UI_WINDOW_MINIPANEL));
	//debug junk
	//std::cout << diablo2::d2_client::get_ui_window_state(diablo2::UI_WINDOW_MINIPANEL)<<" UI window minipanel state\n";
	
	//m_toggle_filter_settings_btn->set_enabled(true);
	//m_toggle_filter_settings_btn->set_visible(true);
	//std::cout << "loot filter settings toggle menu draw\n";
	menu::draw();
}

bool d2_tweaks::client::modules::loot_filter_settings_toggle_menu::key_event(uint32_t key, bool up) {
	if (key == VK_ESCAPE && m_show) {
		m_show = false;

		m_filter_settings_menu->set_enabled(m_show);
		m_filter_settings_menu->set_visible(m_show);

		return true; //block escape key stroke
	}
	//debug junk
	//std::cout << "loot filter settings toggle menu key event\n";
	return menu::key_event(key, up);
}
